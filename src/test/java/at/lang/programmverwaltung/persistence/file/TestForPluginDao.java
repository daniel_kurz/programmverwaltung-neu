package at.lang.programmverwaltung.persistence.file;

import at.lang.programmverwaltung.domain.Plugin;
import at.lang.programmverwaltung.domain.Program;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 29/11/13
 * Time: 08:52
 * To change this template use File | Settings | File Templates.
 */
public class TestForPluginDao {
	private PluginDao pluginDao;

	@Test
	public void writeOnePlugin() {
		pluginDao = new PluginDao("src/test/resources");

		File exists = new File(pluginDao.fileName());
		if(exists.exists()){
			exists.delete();
		}

		Program program = new Program("Adobe Photoshop CS6", "Adobe");
		Plugin plugin = new Plugin(program, "new Brushes", "Adobe");
		ArrayList<Plugin> plugins = new ArrayList<Plugin>();
		plugins.add(plugin);

		pluginDao.writeAll(plugins);
	}

	@Test
	public void readPlugins() {
		pluginDao = new PluginDao("src/test/resources");

		Iterable<Plugin> plugins = pluginDao.readAll();

		System.out.println(plugins);
	}
}
