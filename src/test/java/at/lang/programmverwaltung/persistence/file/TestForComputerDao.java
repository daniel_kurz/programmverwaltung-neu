package at.lang.programmverwaltung.persistence.file;

import java.io.File;

import at.lang.programmverwaltung.domain.Computer;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:38
 * To change this template use File | Settings | File Templates.
 */
public class TestForComputerDao {
	private ComputerDao computerDao;

	@Test
	public void writingOneComputer() {
		computerDao = new ComputerDao("src/test/resources");

		File exists = new File(computerDao.fileName());
		if (exists.exists()) {
			exists.delete();
		}

		Computer c = new Computer("MacBookPro", "Klasse");

		ArrayList<Computer> list = new ArrayList<Computer>();
		list.add(c);

		computerDao.writeAll(list);
	}

	@Test
	public void readAllComputers() {
		computerDao = new ComputerDao("src/test/resources");
		Iterable<Computer> computers = computerDao.readAll();

		System.out.println("computers " + computers);
	}
}
