package at.lang.programmverwaltung.persistence.file;

import at.lang.programmverwaltung.domain.Updates;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 29/11/13
 * Time: 09:44
 * To change this template use File | Settings | File Templates.
 */
public class TestForUpdateDao {
	private UpdatesDao updatesDao;

	@Test
	public void writeOneUpdate() {
		updatesDao = new UpdatesDao("src/test/resources");

		File exists = new File(updatesDao.fileName());
		if (exists.exists()) {
			exists.delete();
		}

		Updates u = new Updates(new Date(), "1.0.2", "Bug fixing");
		ArrayList<Updates> list = new ArrayList<>();
		list.add(u);

		updatesDao.writeAll(list);
	}

	@Test
	public void readAllUpdates() {
		updatesDao = new UpdatesDao("src/test/resources");

		Iterable<Updates> updates = updatesDao.readAll();

		System.out.println(updates);
	}
}
