package at.lang.programmverwaltung.persistence.file;

import at.lang.programmverwaltung.domain.Program;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 29/11/13
 * Time: 09:41
 * To change this template use File | Settings | File Templates.
 */
public class TestForProgramDao {
	private ProgramDao programDao;

	@Test
	public void writeOneProgram() {
		Program p = new Program("Photoshop CS6", "Adobe");

		File exists = new File(programDao.fileName());
		if (exists.exists()) {
			exists.delete();
		}

		ArrayList<Program> list = new ArrayList<Program>();
		list.add(p);

		programDao.writeAll(list);
	}
}
