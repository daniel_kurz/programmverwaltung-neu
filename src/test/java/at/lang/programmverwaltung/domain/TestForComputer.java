package at.lang.programmverwaltung.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 08/11/13
 * Time: 08:23
 * To change this template use File | Settings | File Templates.
 */

import at.lang.programmverwaltung.domain.Computer;
import at.lang.programmverwaltung.domain.Program;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junitparams.JUnitParamsRunner.$;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.is;


@RunWith(JUnitParamsRunner.class)
public class TestForComputer {

	private String setupPlace;
	private String setupName;

	@Before
	public void setup(){
		setupName = "mein Rechner";
		setupPlace = "Zuhause";
	}


	@Test(expected = IllegalArgumentException.class)
	@Parameters
	public void initWithNull(String name, String place){
		new Computer(name, place);
	}

	@SuppressWarnings("unused")
	private Object[] parametersForInitWithNull() {
		return $(
			$(null, "Arbeitsplatz"),
			$("MacBook", null)
		);
	}
	@Test
	public void correctValueIsStored(){
		Computer c = new Computer(setupName, setupPlace);

		assertThat(setupName, is(c.getName()));
		assertThat(setupPlace, is(c.getPlace()));
	}

	public void twoComputersAreEqual(){
		Computer c1 = new Computer(setupName, setupPlace);
		Computer c2 = new Computer(setupName, setupPlace);

		assertThat(c1, is(c2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void checkAddProgramWithNull(){
		Computer c = new Computer(setupName, setupPlace);
		c.addProgram(null);
	}

	@Test
	public void checkAddProgramIfEqual(){
		Computer c = new Computer(setupName, setupPlace);
		Program p = new Program("Photoshop CS6", "Adobe");

		c.addProgram(p);

		ArrayList<Program> programs = c.getPrograms();
		assertThat(p, is(programs.get(0)));
	}
}
