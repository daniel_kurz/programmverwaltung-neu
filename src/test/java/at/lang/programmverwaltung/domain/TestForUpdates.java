package at.lang.programmverwaltung.domain;

import at.lang.programmverwaltung.domain.Updates;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import org.junit.runner.RunWith;
import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 09/11/13
 * Time: 12:45
 * To change this template use File | Settings | File Templates.
 */
@RunWith(JUnitParamsRunner.class)
public class TestForUpdates {
	private Date testDate;
	private String testVersion;
	private String testChanges;

	@Before
	public void setup(){
		testDate = new Date();
		testVersion = "1.0.300";
		testChanges = "Bug fixes";
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters
	public void initWithNull(Date date, String version, String changes){
		new Updates(date, version, changes);
	}

	@SuppressWarnings("unused")
	private Object[] parametersForInitWithNull() {
		return $(
			$(null, "1.0.0", "Nothing"),
			$(new Date(), null, "Nothing"),
			$(new Date(), "1.0.0", null)
		);
	}

	@Test
	public void attributesEqual(){
		Updates u = new Updates(testDate, testVersion, testChanges);

		assertThat(u.getDate(), is(testDate));
		assertThat(u.getVersion(), is(testVersion));
		assertThat(u.getChanges(), is(testChanges));
	}

}
