package at.lang.programmverwaltung.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 08/11/13
 * Time: 09:24
 * To change this template use File | Settings | File Templates.
 */

import at.lang.programmverwaltung.domain.Plugin;
import at.lang.programmverwaltung.domain.Program;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

@RunWith(JUnitParamsRunner.class)
public class TestForPlugin {

	@Test(expected = IllegalArgumentException.class)
	@Parameters
	public void initWithNull(Program program, String name, String publisher){
		new Plugin(program, name, publisher);
	}

	@SuppressWarnings("unused")
	private Object[] parametersForInitWithNull() {
		return $(
			$(null, "AdBlockerPlus", "unknown"),
			$(new Program("Chrome", "Google"), null, "unknown"),
			$(new Program("Chrome", "Google"), "AdBlockerPlus", null)
		);
	}
}
