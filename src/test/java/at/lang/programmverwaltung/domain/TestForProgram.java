package at.lang.programmverwaltung.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 09/11/13
 * Time: 12:38
 * To change this template use File | Settings | File Templates.
 */


import at.lang.programmverwaltung.domain.Program;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junitparams.JUnitParamsRunner.$;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.is;

@RunWith(JUnitParamsRunner.class)
public class TestForProgram {

	private String testName, testPublisher;

	@Before
	public void setup(){
		testName = "Photoshop CS6";
		testPublisher = "Adobe";
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters
	public void initWithNull(String name, String publisher){
		new Program(name, publisher);
	}

	@SuppressWarnings("unused")
	private Object[] parametersForInitWithNull() {
		return $(
				$(null, "Adobe"),
				$("Photoshop CS6", null)
		);
	}

	@Test
	public void PropertiesEqual(){
		Program p = new Program(testName, testPublisher);

		assertThat(p.getName(), is(testName));
		assertThat(p.getPublisher(), is(testPublisher));
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNullUpdate(){
		Program p = new Program(testName, testPublisher);

		p.addUpdate(null);
	}
}
