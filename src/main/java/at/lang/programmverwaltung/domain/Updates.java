package at.lang.programmverwaltung.domain;

import java.io.Serializable;
import java.util.Date;

public class Updates implements Serializable {
	private Date date;
	private String version;
	private String changes;

	public Updates(Date date, String version, String changes) {
		Utility.checkAsset("date", date);
		Utility.checkAsset("version", version);
		Utility.checkAsset("changes", changes);
		this.date = (Date) date.clone();
		this.version = version;
		this.changes = changes;
	}

	public Date getDate() {
		return (Date) date.clone();
	}

	public String getVersion() {
		return version;
	}

	public String getChanges() {
		return changes;
	}
}
