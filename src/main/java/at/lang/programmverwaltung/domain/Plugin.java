package at.lang.programmverwaltung.domain;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 08/11/13
 * Time: 08:11
 * To change this template use File | Settings | File Templates.
 */
public class Plugin extends Program implements Serializable{
	private Program program;

	public Plugin(Program program, String name, String publisher) {
		super(name, publisher);
		Utility.checkAsset("program", program);
		this.program = program;
	}
}
