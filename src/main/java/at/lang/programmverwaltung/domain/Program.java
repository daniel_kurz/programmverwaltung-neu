package at.lang.programmverwaltung.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class Program implements Serializable{
	private String name;
	private String publisher;
	private ArrayList<Updates> updates;

	public Program(String name, String publisher) {
		Utility.checkAsset("Name", name);
		Utility.checkAsset("Publisher", publisher);
		this.name = name;
		this.publisher = publisher;
		updates = new ArrayList<Updates>();
	}

	public void addUpdate(Updates update) {
		Utility.checkAsset("update", update);
		updates.add(update);
	}

	public String getName() {
		return name;
	}

	public String getPublisher() {
		return publisher;
	}
}
