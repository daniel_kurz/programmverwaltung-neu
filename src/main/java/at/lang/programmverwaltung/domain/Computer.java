package at.lang.programmverwaltung.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class Computer implements Serializable {
	/**
	 * Contains all the programs on one computer.
	 */
	private ArrayList<Program> programs;
	/**
	 * Name of the Computer.
	 */
	private String name;
	/**
	 * Location of the Computer.
	 */
	private String place;

	/**
	 * @param name  Name of the Computer (used to identify it)
	 * @param place Describes where the Computer is located
	 */
	public Computer(String name, String place) {
		Utility.checkAsset("name", name);
		Utility.checkAsset("place", place);
		this.name = name;
		this.place = place;
		this.programs = new ArrayList<Program>();
	}

	/**
	 * @param program Program that gets added to programs
	 */
	public void addProgram(Program program) {
		Utility.checkAsset("program", program);
		programs.add(program);
	}

	/**
	 * @return Name of the Computer
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return Location of the Computer
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @return List of Programs installed on this Computer
	 */
	public ArrayList<Program> getPrograms() {
		return (ArrayList) programs.clone();
	}
}
