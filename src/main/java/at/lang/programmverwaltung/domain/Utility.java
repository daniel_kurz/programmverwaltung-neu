package at.lang.programmverwaltung.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 16.10.13
 * Time: 09:24
 * To change this template use File | Settings | File Templates.
 */
public class Utility {
	public static void checkAsset(String variableName, Object variable) {
		if (variable == null) {
			throw new IllegalArgumentException(variableName + " must be set!");
		}
	}

	public static void checkAsset(String variableName, String variable) {
		if (variable == null) {
			throw new IllegalArgumentException(variableName + " must be set!");
		} else {
			if (variable.isEmpty()) {
				throw new IllegalArgumentException(variableName + " must not be empty!");
			}
		}
	}
}

