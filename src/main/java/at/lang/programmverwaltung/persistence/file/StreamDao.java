package at.lang.programmverwaltung.persistence.file;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:06
 * To change this template use File | Settings | File Templates.
 */
public interface StreamDao<T> {
	Iterable<T> readAll();
	void writeAll(Iterable<T> objects);
}
