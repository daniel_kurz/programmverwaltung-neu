package at.lang.programmverwaltung.persistence.file;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:27
 * To change this template use File | Settings | File Templates.
 */

import java.io.File;

import at.lang.programmverwaltung.domain.Computer;

public class ComputerDao extends AbstractStreamDao<Computer>{
	private static final String fileName = "computers";
	private final String directoryName;

	public ComputerDao(String directoryName) {
		this.directoryName = directoryName;
	}

	@Override
	public String fileName() {
		return directoryName + File.separator + fileName;
	}
}
