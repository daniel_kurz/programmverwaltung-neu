package at.lang.programmverwaltung.persistence.file;

import at.lang.programmverwaltung.domain.Updates;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:36
 * To change this template use File | Settings | File Templates.
 */
public class UpdatesDao extends AbstractStreamDao<Updates> {

	private static final String fileName = "updates";
	private final String directoryName;

	public UpdatesDao(String directoryName) {
		this.directoryName = directoryName;
	}

	@Override
	public String fileName() {
		return directoryName + File.separator + fileName;
	}
}
