package at.lang.programmverwaltung.persistence.file;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:04
 * To change this template use File | Settings | File Templates.
 */

import java.io.*;

public abstract class AbstractStreamDao<T> implements StreamDao<T> {
	public abstract String fileName();

	@Override
	public Iterable<T> readAll() {
		try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName()))) {
			return (Iterable<T>) objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
			throw new FilePersistenceException("Failed to read objects", e);
		}
	}

	@Override
	public void writeAll(Iterable<T> objects) {
		try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName()))) {
			objectOutputStream.writeObject(objects);
		} catch (IOException e) {
			throw new FilePersistenceException("Failed to write objects", e);
		}
	}
}
