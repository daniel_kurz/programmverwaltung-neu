package at.lang.programmverwaltung.persistence.file;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:11
 * To change this template use File | Settings | File Templates.
 */
public class FilePersistenceException extends RuntimeException{
	public FilePersistenceException(String message){
		super(message);
	}

	public FilePersistenceException(String message, Throwable cause) {
		super(message, cause);
	}
}
