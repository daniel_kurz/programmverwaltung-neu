package at.lang.programmverwaltung.persistence.file;

import at.lang.programmverwaltung.domain.Program;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:34
 * To change this template use File | Settings | File Templates.
 */
public class ProgramDao extends AbstractStreamDao<Program> {

	private static final String fileName = "programs";

	private final String directoryName;

	public ProgramDao(String directoryName) {
		this.directoryName = directoryName;
	}

	@Override
	public String fileName() {
		return directoryName + File.separator + fileName;
	}
}
