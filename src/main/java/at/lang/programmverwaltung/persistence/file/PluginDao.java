package at.lang.programmverwaltung.persistence.file;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel
 * Date: 27/11/13
 * Time: 08:32
 * To change this template use File | Settings | File Templates.
 */

import java.io.File;

import at.lang.programmverwaltung.domain.Plugin;

public class PluginDao extends AbstractStreamDao<Plugin> {
	private static final String fileName = "plugins";

	private final String directoryName;

	public PluginDao(String directoryName) {
		this.directoryName = directoryName;
	}

	@Override
	public String fileName() {
		return directoryName + File.separator + fileName;
	}
}
